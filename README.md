# Flappy Bird
For our final project in QML we desided to make a flappybird clone.

## How to play  
To start the game, simply press the "start" button.  
After that, press the screen to flap your wings. Avoid hitting the pipes for as long as you can, and get the highest score!  
Forgetting to flap and hitting the ground will also end the game.

## Screenshots from game

![start page](/screenshot/startPage.png)

![Get Ready](/screenshot/getReadyScreen.png)

![Playing](/screenshot/playingPage.png)

![Game Over](/screenshot/gameOverPage.png)

If you get a score higher than 10 you get rewarded with a medal! \
![Game Over With Medal](/screenshot/gameOverWithMedal.png)

## Contributors
* Alexander Øvergård
* Eivind Vold Aunebakk
* Lucas Emmes
