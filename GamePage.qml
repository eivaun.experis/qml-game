import QtQuick
import QtMultimedia
import QtQuick.Controls
import Qt.labs.settings

Page {
    id: gamePage

    property int staticCEILING: 0;
    property int staticFLOOR: 256-30;

    property int score: 0;
    property int highScore: 0;
    property int scorePos: 73;
    property int pointsGained: 1;
    property int gravity: 1;
    property int jumpAcc: 7;
    property int birdXPos: width/4

    property int movementSpeed: 3
    property int frameTime: 33

    property bool gameOver: false

    Settings {
        property alias highScore: gamePage.highScore
    }

    SoundEffect {
        id: wingSound
        source: Qt.resolvedUrl("qrc:/sound/wing.wav")
    }
    SoundEffect {
        id: hitSound
        source: Qt.resolvedUrl("qrc:/sound/hit.wav")
    }
    SoundEffect {
        id: pointSound
        source: Qt.resolvedUrl("qrc:/sound/point.wav")
    }
    SoundEffect {
        id: dieSound
        source: Qt.resolvedUrl("qrc:/sound/die.wav")
    }
    SequentialAnimation on x {
        id:delayedDieSound
        running: false
        PauseAnimation {
            duration: 500
        }
        ScriptAction {
            script: { dieSound.play() }
        }
    }

    MouseArea {
        id: screenPress;
        anchors.fill: parent;
        onClicked: {
            if (!gameLoop.running) {
                startGame();
            }
            bird.jump();
            wingSound.play();
        }
    }

    NumberAnimation {
        id: hideLogo
        targets: [howToPlay, getReadyLogo]
        to: 0
        properties: "opacity"
        duration: 200
    }

    Image {
        id: background
        source: "/flappy-bird-spritesheet.png"
        sourceClipRect: Qt.rect(0, 0, 144, 256)
    }

    Image {
        id: howToPlay
        anchors.centerIn: background
        source: "/flappy-bird-spritesheet.png"
        sourceClipRect: Qt.rect(292, 91, 57, 49)
    }

    Image {
        id: getReadyLogo
        anchors.margins: 10
        anchors.bottom: howToPlay.top
        anchors.horizontalCenter: background.horizontalCenter
        source: "/flappy-bird-spritesheet.png"
        sourceClipRect: Qt.rect(295, 59, 92, 25)
    }

    TextBitmap {
        id: scoreLabel
        text: score
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
        z: 100
    }

    function startGame() {
        hideLogo.start()
        gameLoop.running = true;
        groundAnim.start()
        bird.running = true
    }

    function endGame() {
        hitSound.play()
        if(bird.y < staticFLOOR / 2)
            delayedDieSound.start();

        gameOver = true
        screenPress.enabled = false;
        bird.velocity = 0;
        gamePage.pointsGained = 0;
        groundAnim.stop()
        bird.running = false
        scoreLabel.visible = false;
        flashScreenAnim.start();

        if(score > highScore) {
            highScore = score
        }
        gameOverPanel.showPlanel(score, highScore);
    }

    SequentialAnimation on x {
        id: gameLoop;
        PauseAnimation { duration: frameTime; }
        running: false;
        loops: Animation.Infinite;
        ScriptAction {
            script: {
                gamePage.gravityLoop();
                if(!gameOver) {
                    gamePage.movePipes();
                    gamePage.checkCollision();
                    gamePage.scoreCheck();
                }
            }
        }
    }

    function scoreCheck() {
        scorePos += movementSpeed;
        if (scorePos >= 115) {
            scorePos = 0;
            score += pointsGained;
            pointSound.play();
        }
    }

    function gravityLoop() {
        bird.velocity += gravity;
        bird.y += bird.velocity;
        if (bird.y > gamePage.staticFLOOR - bird.height) {bird.y = gamePage.staticFLOOR - bird.height}
        if (bird.y < gamePage.staticCEILING) {bird.y = gamePage.staticCEILING}
    }

    function clamp(number, min, max) {
        return Math.max(min, Math.min(number, max));
    }

    function checkOverlap(rectangleLeft,  rectangleTop, rectangleWidth, rectangleHeight, circleCenterX, circleCenterY, circleRadius) {
        // Find the closest point to the circle within the rectangle
        let closestX = clamp(circleCenterX, rectangleLeft, rectangleLeft + rectangleWidth);
        let closestY = clamp(circleCenterY, rectangleTop, rectangleTop + rectangleHeight);

        // Calculate the distance between the circle's center and this closest point
        let distanceX = circleCenterX - closestX;
        let distanceY = circleCenterY - closestY;

        // If the distance is less than the circle's radius, an intersection occurs
        let distanceSquared = (distanceX * distanceX) + (distanceY * distanceY);
        return distanceSquared < (circleRadius * circleRadius);
    }

    function checkCollision() {
        for (let i = 0; i < pipeRepeater.count; i++) {
            let currentPipe = pipeRepeater.itemAt(i);

            if(checkOverlap(currentPipe.x, currentPipe.y, currentPipe.width, currentPipe.height, bird.x + bird.width/2,bird.y + bird.height/2, bird.height/2) ||
                    (bird.y + bird.height >= gamePage.staticFLOOR) ) {
                endGame();
                break;
            }
        }
    }

    function movePipes() {
        for (let i = 0; i < pipeRepeater.count; i+=2) {
            let top = pipeRepeater.itemAt(i);
            let bottom = pipeRepeater.itemAt(i+1);

            if (top.x < -gamePage.width*0.6) {
                top.x = gamePage.width;
                bottom.x = gamePage.width;

                top.y = - 160 + ~~(Math.random() * 0.5 * gamePage.height);
                bottom.y = top.y + 160 + ~~(0.5 * gamePage.height);
            }
            top.x -= movementSpeed;
            bottom.x -= movementSpeed;
        }
    }

    Repeater {
        id: pipeRepeater;
        model: [[0,-120,160, 56], [0,200,160, 84], [0+gamePage.width*0.8, -120, 160, 56], [0+gamePage.width*0.8, 200, 160, 84]]
        Image {
            x: modelData[0];
            y: modelData[1];
            source: "/flappy-bird-spritesheet.png"
            sourceClipRect: Qt.rect(modelData[3], 323, 26, 160)
        }
    }

    Image {
        id: ground
        x:0
        y:staticFLOOR
        source: "/flappy-bird-spritesheet.png"
        sourceClipRect: Qt.rect(292, 0, 168, 56)
        SequentialAnimation on x {
            id: groundAnim
            running: false
            loops: Animation.Infinite
            PropertyAnimation { to: -24; duration: (24/movementSpeed)*frameTime }
            PropertyAction { value: 0 }
        }
    }

    AnimatedSprite {
        id: bird;
        x: birdXPos;
        y: (gamePage.staticFLOOR - gamePage.staticCEILING) / 2;
        rotation: 0;

        source: "/flappy-bird-spritesheet.png"
        frameX: 146
        frameY: 481
        frameWidth: 21
        frameHeight: 14
        frameCount: 4
        frameRate: 5
        interpolate: false
        running: false

        property int velocity: -jumpAcc;
        function jump() {
            bird.velocity = -jumpAcc;
            rotation = -30;
            tiltAnim.restart()
        }

        NumberAnimation on rotation {
            id: tiltAnim
            running: false
            duration: 750;
            to: 40;
        }
    }

    GameOverPanel {
        id: gameOverPanel
    }

    Rectangle {
        id: flash
        anchors.fill: parent
        color: "white"
        opacity: 0

        NumberAnimation on opacity {
            id: flashScreenAnim
            running: false
            from: 1
            to: 0
            duration: 500
        }
    }
}

