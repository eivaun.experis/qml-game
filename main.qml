import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQml.StateMachine as DSM
import QtMultimedia

Window {
    width: 144*3
    height: 256*3
    visible: true
    title: qsTr("Flappy Bird")

    property real windowScale: Math.min(height/256, width/144)

    SoundEffect {
        id: swooshingSound
        source: Qt.resolvedUrl("qrc:/sound/swooshing.wav")
    }

    Item {
        width:144
        height:256
        scale: windowScale
        transformOrigin: Item.TopLeft
        StackView {
            id: stack
            anchors.fill: parent
            initialItem: StartPage {}

            pushEnter: Transition {
                ScriptAction {
                    script: swooshingSound.play();
                }

                XAnimator {
                    from: stack.width
                    to: 0
                    duration: 400
                    easing.type: Easing.OutCubic
                }
            }
            pushExit: Transition {
                XAnimator {
                    from: 0
                    to: -stack.width
                    duration: 400
                    easing.type: Easing.OutCubic
                }
            }

            replaceEnter: Transition {
                ScriptAction {
                    script: swooshingSound.play();
                }

                XAnimator {
                    from: stack.width
                    to: 0
                    duration: 400
                    easing.type: Easing.OutCubic
                }
            }
            replaceExit: Transition {
                XAnimator {
                    from: 0
                    to: -stack.width
                    duration: 400
                    easing.type: Easing.OutCubic
                }
            }
        }
    }
}
