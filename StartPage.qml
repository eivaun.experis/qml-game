import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Page {
    id: startPage

    Image {
        id: background
        source: "/flappy-bird-spritesheet.png"
        sourceClipRect: Qt.rect(0, 0, 144, 256)
    }
    Image {
        id: ground
        y:256-30
        source: "/flappy-bird-spritesheet.png"
        sourceClipRect: Qt.rect(292, 0, 168, 56)
        SequentialAnimation on x {
            running: true
            loops: Animation.Infinite
            PropertyAnimation { to: -24; duration: 500 }
            PropertyAction { value: 0 }
        }
    }
    ColumnLayout
    {
        anchors.fill: parent
        spacing: 20
        Item { Layout.fillHeight: true }
        Image {
            Layout.alignment:Qt.AlignHCenter
            id: floppyBirdLogo
            source: "/flappy-bird-spritesheet.png"
            sourceClipRect: Qt.rect(351, 91, 89, 24)
        }
        Item
        {
            Layout.alignment:Qt.AlignHCenter
            width: redBird.frameWidth
            height: redBird.frameHeight + redBird.bobHeight
            AnimatedSprite {
                property int bobHeight: 10
                id: redBird
                source: "/flappy-bird-spritesheet.png"
                frameX: 146
                frameY: 481
                frameWidth: 21
                frameHeight: 14
                frameCount: 4
                frameRate: 5
                interpolate: false
                running: true
                SequentialAnimation on y {
                    running: true
                    loops: Animation.Infinite
                    PropertyAnimation { to: redBird.bobHeight; duration: 500 }
                    PropertyAnimation { to: 0; duration: 500 }
                }
            }
        }
        RowLayout
        {
            Layout.alignment:Qt.AlignHCenter
            Layout.fillWidth: true
            spacing: 20
            Image {
                id: startButton
                source: "/flappy-bird-spritesheet.png"
                sourceClipRect: Qt.rect(354, 118, 52, 29)
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        stack.push("GamePage.qml")
                    }
                }
            }
            Image {
                id: scoreButton
                source: "/flappy-bird-spritesheet.png"
                sourceClipRect: Qt.rect(414, 118, 52, 29)
            }
        }
        Item { Layout.fillHeight: true }
    }
}
