import QtQuick
import QtMultimedia
import QtQuick.Layouts
import QtQuick.Controls

Item {

    id: gameOverPanel

    width: parent.width
    height: parent.height

    y: height

    function showPlanel(score, highScore) {
        showPanelAnim.start();
        currentScoreLabel.text = score
        highScoreLabel.text = highScore

        if(score < 10){
            medal.sourceClipRect= Qt.rect(160, 269, 22, 22)
        }
        else if(score >= 40){
            medal.sourceClipRect= Qt.rect(121, 258, 22, 22)
        }
        else if(score >= 30){
            medal.sourceClipRect= Qt.rect(121, 282, 22, 22)
        }
        else if(score >= 20){
            medal.sourceClipRect= Qt.rect(112, 453, 22, 22)
        }
        else if(score >= 10){
            medal.sourceClipRect= Qt.rect(112, 477, 22, 22)
        }
    }

    SoundEffect {
        id: swooshingSound
        source: Qt.resolvedUrl("qrc:/sound/swooshing.wav")
    }

    SequentialAnimation on y {
        id: showPanelAnim
        running: false
        PauseAnimation {
            duration: 1000
        }
        ScriptAction {
            script: { swooshingSound.play(); }
        }
        NumberAnimation {
            to: 0
            duration: 500
        }
    }

    ColumnLayout
    {
        anchors.fill: parent
        spacing: 20
        Item { Layout.fillHeight: true }

        Image {
            Layout.alignment:Qt.AlignHCenter
            id: gameOverLogo
            source: "/flappy-bird-spritesheet.png"
            sourceClipRect: Qt.rect(395, 59, 96, 21)
        }
        Image {
            Layout.alignment: Qt.AlignHCenter
            id: scoreBoard
            source: "/flappy-bird-spritesheet.png"
            sourceClipRect: Qt.rect(3, 259, 113, 57)
            TextBitmap {
                id: currentScoreLabel
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.rightMargin: 10
                anchors.topMargin: 13
                transformOrigin: Item.Right
                scale: 0.5
                text: "10000"
            }
            TextBitmap {
                id: highScoreLabel
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.rightMargin: 10
                anchors.bottomMargin: 6
                transformOrigin: Item.Right
                scale: 0.5
                text: "10000"
            }
            Image {
                id: medal
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.topMargin: 21
                anchors.leftMargin: 13
                source: "/flappy-bird-spritesheet.png"
                sourceClipRect: Qt.rect(112, 477, 22, 22)
            }
        }

        RowLayout
        {
            Layout.alignment:Qt.AlignHCenter
            Layout.fillWidth: true
            spacing: 20
            Image {
                id: startButton
                source: "/flappy-bird-spritesheet.png"
                sourceClipRect: Qt.rect(354, 118, 52, 29)
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        stack.replace(stack.currentItem, "GamePage.qml")
                    }
                }
            }
            Image {
                id: scoreButton
                source: "/flappy-bird-spritesheet.png"
                sourceClipRect: Qt.rect(414, 118, 52, 29)
            }
        }
        Item { Layout.fillHeight: true }
    }
}
