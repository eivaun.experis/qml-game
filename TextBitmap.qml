// https://stackoverflow.com/questions/16404657/bitmap-fonts-in-qt-quick-qml

import QtQuick

Row {
    property string text: ""
    Repeater {
        model: text.length
        Image {
            source: "qrc:/bitmapfont/" + text.charCodeAt(index) + ".png"
        }
    }
}
